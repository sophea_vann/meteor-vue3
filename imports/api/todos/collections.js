import { Mongo } from "meteor/mongo";
import SimpleSchema from "simpl-schema";
const Todos = new Mongo.Collection("todos");

Todos.schema = new SimpleSchema({
  title: String,
  body: {
    type: String,
    optional: true,
  },
  createdAt: {
    type: Date,
    optional: true,
    defaultValue: new Date(),
  },
  updatedAt: {
    type: Date,
    optional: true,
    defaultValue: new Date(),
  },
});

Todos.attachSchema(Todos.schema);

export default Todos;

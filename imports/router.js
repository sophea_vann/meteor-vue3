import { createRouter, createWebHistory } from 'vue-router'

import Home from './pages/Home.vue'

const routes = [
  {
    path: '/',
    component: Home,
    name: 'home',
  },
  {
    path: '/todos',
    component: () => import('./pages/Todos.vue'),
    name: 'todos',
  },
  {
    path: '/item',
    component: () => import('./pages/Item.vue'),
    name: 'item',
  },
]

const router = createRouter({
  routes,
  history: createWebHistory(),
})

export default router
